*** Settings ***
Suite Setup       Open Browser To Homepage
#Suite Teardown    Close All Browsers

Resource         ../resources/resource_webshopbe.robot
Resource         ../resources/variables_webshopbe.robot

#Command:
#   robot -d results -v ENVIRONMENT:live -v BROWSER:chrome tests\01_send_order.robot

*** Test Cases ***
Customer should be able to successfully login
    Open Login Page
    Complete With Valid Dates And Submit User Login

Customer should be able to add a product to cart from homepage
    Select Product And Add To Cart From Homepage

Customer should be able to add a product to cart from browse
    Open Browse Categories From Header
    Add To Cart A Product From Browse

Customer should be able to add a product to cart from details page
    Select A Random Product From Browse
    Add Product To Cart

Customer should be able to search a product and add to cart
# @Marius Nu pot sa caut un produs in asa fel in cat sa ajung pe
# pagina de search pentru ca produsele nu sunt afisate in ordinea cautarii
    Select Name Of The Product And Search It Use Autocomplete
    Add Product To Cart

Customer should be able to check the products details from cart
    Go To Cart
    Check The Price For Products

Customer should be able to submit the Overview and remove the cart
    Submit The Terms And Conditions
    Remove The Cart

